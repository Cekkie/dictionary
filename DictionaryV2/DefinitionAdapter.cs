using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace DictionaryV2
{
    class DefinitionAdapter : BaseAdapter
    {

        private Activity activity;
        private List<Word> list;

        public DefinitionAdapter(Activity activity, List<Word> list)
        {
            this.activity = activity;
            this.list = list;
        }

        public override int Count
        {
            get
            {
                return list.Count;
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            throw new NotImplementedException();
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if (view == null)
            {
                view = activity.LayoutInflater.Inflate(Resource.Layout.Word, parent, false);
                Word item = this[position];
                view.FindViewById<TextView>(Resource.Id.dictionary).Text = item.dictionary;
                view.FindViewById<TextView>(Resource.Id.description).Text = item.description;


            }
            return view;
        }



        public Word this[int index]
        {
            get { return list[index]; }
        }
    }
}