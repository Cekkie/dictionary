using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace DictionaryV2
{
    [Serializable]
    class MemoryObject
    {
        public List<Word> words { get; set; }
        public List<Dictionary> dictionaries { get; set; }

        public MemoryObject()
        {
            words = new List<Word>();
            dictionaries = new List<Dictionary>();
        }
    }
}