using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace DictionaryV2
{
    [Serializable]
    class Dictionary
    {
        public string id { get; set; }
        public string dict { get; set; }
        public bool isChecked { get; set; }

        public Dictionary( string _id, string _dict, bool _isChecked)
        {
            id = _id;
            dict = _dict;
            isChecked = _isChecked;
        }
    }
}