﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Dictionary
{
	[Activity (Label = "LookupActivity")]			
	public class LookupActivity : Activity
	{

		private EditText text;
		private Button search;
		ListView Result;


		private ISharedPreferences data;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);

			text = (EditText)FindViewById (Resource.Id.text);
			search = (Button)FindViewById (Resource.Id.search);


			search.Click += (sender, EventArgs) => searchWord();

		}

		private void searchWord(){
			string input = text.Text;
			if (input != ""){
			XmlDocument doc = WebClient.sendGetRequest ("http://service.aonaware.com/DictService/DictService.asmx", "Define", "word", input);
			XmlNodeList docList = doc.GetElementsByTagName ("Definition");
				Console.WriteLine ("hier geraak ik");
			}
		}

		protected override void OnResume ()
		{
			base.OnResume ();
		}

		protected override void OnStop ()
		{
			base.OnStop ();
		}

		protected override void OnPause ()
		{
			base.OnPause ();
		}

		protected override void OnStart ()
		{
			base.OnStart ();
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
		}
	}
}

