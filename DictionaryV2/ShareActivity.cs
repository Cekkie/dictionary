using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;
using Newtonsoft.Json;

namespace DictionaryV2
{
    [Activity(Label = "ShareActivity")]
    public class ShareActivity : Activity
    {
        private EditText email;
        private Button share;
        private Button menu;
        private MemoryObject memObj;
        
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.ShareView);
            memObj = new MemoryObject();
            memObj.words = new List<Word>();
            email = (EditText)FindViewById(Resource.Id.email);
            share = (Button)FindViewById(Resource.Id.share);
            menu = (Button)FindViewById(Resource.Id.menu);

            menu.Click += (s, arg) => {
                PopupMenu pMenu = new PopupMenu(this, menu);
                pMenu.Inflate(Resource.Layout.Menu);
                pMenu.Show();
                pMenu.MenuItemClick += (object sender, PopupMenu.MenuItemClickEventArgs e) => ClickMenu(sender, e);
            };

            share.Click += (sender, EventArgs) => ShareDefinitions();
        }

        private void ShareDefinitions()
        {
            string text = email.Text;
            if(text != null && memObj.dictionaries.Count > 0)
            {
                Console.WriteLine("ik geraak hier");
                var email = new Intent(Android.Content.Intent.ActionSend);
                email.PutExtra(Android.Content.Intent.ExtraEmail, new string[] { text });
                email.PutExtra(Android.Content.Intent.ExtraSubject, "The definitions of the word: " + memObj.words[0].word);
                email.PutExtra(Android.Content.Intent.ExtraText, ComposeMessage());
                email.SetType("message/rfc822");
                StartActivity(email);
            }
        }

        private string ComposeMessage()
        {
            string res = "";
            foreach (Word w in memObj.words)
            {
                res += " " + w.dictionary + "\n" + w.description + "\n";
            }
            return res;
        }

        protected override void OnResume()
        {

            ISharedPreferences preferences = PreferenceManager.GetDefaultSharedPreferences(this);
            if (preferences.GetString("MemObject", "") != "")
            {
                memObj = JsonConvert.DeserializeObject<MemoryObject>(preferences.GetString("MemObject", ""));
            }

            var otherView = Intent.GetSerializableExtra("MemObject");
            if (otherView != null)
            {

                memObj = JsonConvert.DeserializeObject<MemoryObject>(otherView.ToString());

            }
            base.OnResume();
            }







        private void ClickMenu(object sender, PopupMenu.MenuItemClickEventArgs e)
        {
            string title = e.Item.TitleCondensedFormatted.ToString();
            if (title.Equals("Look up a word"))
            {
                setMainView();
            }
            else if (title.Equals("Share"))
            {
                setDictionaryView();
            }
        }

        private void setDictionaryView()
        {
            var intent = new Intent(this, typeof(DictionaryActivity));
            var serialized = JsonConvert.SerializeObject(memObj);
            intent.PutExtra("MemObject", serialized);
            StartActivity(intent);
        }

        private void setMainView()
        {
            var intent = new Intent(this, typeof(MainActivity));
            var serialized = JsonConvert.SerializeObject(memObj);
            intent.PutExtra("MemObject", serialized);
            StartActivity(intent);
        }


    }
}